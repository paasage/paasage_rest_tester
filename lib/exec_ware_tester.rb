  class ExecWareTester < Thor
    DEFAULT_BASE_URL='http://192.168.65.100:9999/api'
    DEFAULT_USER_NAME='john.doe@example.com'
    DEFAULT_PASSWORD='admin'
    DEFAULT_TENANT='admin'

    def self.base_url
      x = ENV["DEFAULT_BASE_URL"] 
      if x then
        x
      else 
        DEFAULT_BASE_URL
      end
    end
    def self.user_name
      x= ENV["DEFAULT_USER_NAME"] 
      if x then
        x
      else 
        DEFAULT_USER_NAME
      end
    end
    def self.tenant
      x= ENV["DEFAULT_TENANT"] 
      if x then
        x
      else 
        DEFAULT_TENANT
      end
    end
    def self.password
      x= ENV["DEFAULT_PASSWORD"] 
      if x then
        x
      else 
        DEFAULT_PASSWORD
      end
    end

    @@user_id = 0
    def self.user_id
      @@user_id
    end
    @@token  = ''
    def self.token
      @@token
    end
    desc 'create NAME', 'Creates paasage resource named NAME'
    def create(name)
        retrieve_token
	      PaasageModelTester.test_create name
    end

    desc 'update NAME, action', 'Update resource with id and set new action'
    def update(name, action)
      retrieve_token
      PaasageModelTester.test_update name, action
    end

    desc 'get ID', 'Get resource with id id'
    def get(id)
      retrieve_token
      PaasageModelTester.test_get id
    end

    desc 'get_by_name NAME', 'Get resource named <name>'
    def get_by_name(name)
      retrieve_token
      PaasageModelTester.test_get_by_name name
    end

    desc 'get_id_from_name NAME', 'Get resource id of resource named <name>'
    def get_id_from_name(name)
      retrieve_token
      PaasageModelTester.test_get_id_from_name name
    end


    desc 'get_model ID', 'Get model of resource with id id'
    def get_model(id)
      retrieve_token
      PaasageModelTester.test_get_model id
    end


    desc 'upload NAME, file_name', 'upload an xmi file to resource named <name>'
    def upload(name, file_name)
      retrieve_token
      PaasageModelTester.test_upload name, file_name
    end

    private
    def retrieve_token
      auth_payload= { email: "#{ExecWareTester.user_name}", password: "#{ExecWareTester.password}", tenant: "#{ExecWareTester.tenant}" }
      url ="#{ExecWareTester.base_url}/login"
      response = RestClient.post url, JSON.generate(auth_payload), {content_type: 'application/json',accept: 'application/json' }
      #puts response
      response_hash =JSON.parse(response)
      @@token = response_hash['token']
      @@user_id = response_hash['userId']
      #puts response_hash['token']
    end

  end
