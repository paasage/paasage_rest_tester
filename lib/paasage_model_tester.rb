class PaasageModelTester
  public
  def self.test_create(name)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    payload = {
        name: name, state: "CREATED", subState: "empty", action:"CREATE", xmiModelEncoded: "UNCHANGED"
    }
    url = "#{ExecWareTester.base_url}/model"
#    puts '****'
#    puts url
#    puts headers
#    puts payload.to_json
#    puts '****'
    begin
#     puts "about to post"
      response=RestClient.post url, payload.to_json, headers
#      puts response.to_json
      paasage_model = JSON.parse(response)
      puts "name:#{paasage_model["name"]}, action: #{paasage_model["action"]}, state: #{paasage_model["state"]}, subState: #{paasage_model["subState"]}"
    rescue
    rescue => e
      puts e.response.to_json
    end
  end

  def self.test_update(name, action)
    id = test_get_id_from_name(name)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    payload = {
        name: "UNCHANGED", state: "UNCHANGED", subState: "UNCHANGED", action: action, xmiModelEncoded: "UNCHANGED"
    }
    url = "#{ExecWareTester.base_url}/model/#{id}"
    begin
      response=RestClient.put url, payload.to_json, headers
      paasage_model = JSON.parse(response)
      puts "name:#{paasage_model["name"]}, action: #{paasage_model["action"]}, state: #{paasage_model["state"]}, subState: #{paasage_model["subState"]}"
    rescue => e
      puts e.response.to_json
    end

  end

  def self.test_get(id)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    url = "#{ExecWareTester.base_url}/model/#{id}"
    begin
      response = RestClient.get url, headers
#      puts response.to_json
      paasage_model = JSON.parse(response)
      puts "name:#{paasage_model["name"]}, action: #{paasage_model["action"]}, state: #{paasage_model["state"]}, subState: #{paasage_model["subState"]}"
    rescue => e
      puts e.response.to_json
    end
  end

  def self.test_get_by_name(name)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    url = "#{ExecWareTester.base_url}/model/name/#{name}"
    begin
      response = RestClient.get url, headers
 #     puts response.to_json
      paasage_models = JSON.parse(response)
      paasage_model = paasage_models[0]
      id= id_from_model(paasage_model)

      puts "id: #{id}, name:#{paasage_model["name"]}, action: #{paasage_model["action"]}, state: #{paasage_model["state"]}, subState: #{paasage_model["subState"]}"
      paasage_model
    rescue => e
      puts e.response.to_json
    end
  end

  def self.id_from_model(paasage_model)
    link = paasage_model["link"][0]
#    puts link
    href = link["href"]
#    puts href
    id = File.basename(URI.parse(href).path)
#    puts id
    return id

  end

  def self.test_get_id_from_name(name)
    paasage_model = self.test_get_by_name(name)
    return id_from_model(paasage_model)
  end

  def self.test_get_model(name)
    id = test_get_id_from_name(name)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    url = "#{ExecWareTester.base_url}/model/#{id}"
    begin

      response = JSON.parse( RestClient.get( url, headers))

      encoded_file_contents = response['xmiModelEncoded']
      file_contents = Base64.decode64(encoded_file_contents)
      puts file_contents

    rescue => e
      puts e.response.to_json
    end
  end


  def self.test_upload(name, file_name)
    id = test_get_id_from_name(name)
    model_name = case name.downcase
     when 'flexiant' then 'upperware-models/fms/Flexiant'
     when 'gwdg' then 'upperware-models/fms/GWDG'
     when 'amazon' then 'upperware-models/fms/AmazonEC2'
     when 'omistack' then 'upperware-models/fms/Omistack'
     else name
    end

    file_contents = File.read(file_name)
    encoded_file_contents = Base64.strict_encode64(file_contents)
    headers= {'Content-Type': 'application/json','Accept': 'application/json', 'X-Auth-Token': "#{ExecWareTester.token}", 'X-Auth-UserId': "#{ExecWareTester.user_id}", 'X-Tenant':'admin' }
    payload = {
        name: "UNCHANGED", state: "UNCHANGED", subState: model_name, action: "UPLOAD_XMI", xmiModelEncoded: encoded_file_contents
    }
    url = "#{ExecWareTester.base_url}/model/#{id}"
    begin
      response=RestClient.put url, payload.to_json, headers
      paasage_model = JSON.parse(response)
      puts "name:#{paasage_model["name"]}, action: #{paasage_model["action"]}, state: #{paasage_model["state"]}, subState: #{paasage_model["subState"]}"
    rescue => e
      puts e.response.to_json
    end
  end

end
